# noqa Taken from http://nemesisdesign.net/blog/coding/django-filefield-content-type-size-validation/

from django.db.models import FileField
from django.forms import forms
from django.template.defaultfilters import filesizeformat
from django.conf import settings


class ContentTypeRestrictedFileField(FileField):
    def __init__(self, *args, **kwargs):
        self.content_types = kwargs.pop("content_types", None)

        super(ContentTypeRestrictedFileField, self).__init__(*args, **kwargs)

    def clean(self, *args, **kwargs):
        data = super(ContentTypeRestrictedFileField, self).clean(
            *args, **kwargs)

        file = data.file
        content_type = file.content_type
        if content_type in self.content_types:
            # pylint: disable=protected-access
            if file._size > settings.BACKWORTH_MAX_APP_BUNDLE_SIZE:
                # pylint: disable=protected-access
                raise forms.ValidationError(
                    'Please keep filesize under %s. Current filesize %s' %
                    (filesizeformat(
                        settings.BACKWORTH_MAX_APP_BUNDLE_SIZE),
                     filesizeformat(file._size)))
        else:
            raise forms.ValidationError(
                'Filetype not supported, accepted types: %s' %
                str(self.content_types))

        return data
