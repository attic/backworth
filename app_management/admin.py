from django.contrib import admin
from .models import App, AppVersion


class AppVersionInline(admin.TabularInline):
    model = AppVersion
    extra = 0
    readonly_fields = \
        ('semantic_version', 'uploader', 'uploaded_date', 'app', 'tarball_link')
    fields = ('semantic_version', 'uploader', 'uploaded_date', 'app', 'tarball_link',
              'accepted')


class AppAdmin(admin.ModelAdmin):
    inlines = [
        AppVersionInline,
    ]
    readonly_fields = ('title', 'author', 'created_date')


class AppVersionAdmin(admin.ModelAdmin):
    readonly_fields = \
        ('semantic_version', 'uploader', 'uploaded_date', 'app', 'tarball_link')
    fields = ('semantic_version', 'uploader', 'uploaded_date', 'app', 'tarball_link',
              'accepted')

admin.site.register(App, AppAdmin)
admin.site.register(AppVersion, AppVersionAdmin)
