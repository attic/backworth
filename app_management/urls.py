from django.conf.urls import url

from . import views

app_name = 'app'
urlpatterns = [
    url(r'^create', views.AppCreateView.as_view(), name='app-create'),
    url(r'^update/(?P<pk>\d+)',
        views.AppUpdateView.as_view(),
        name='app-update'),
    url(r'^new_version/(?P<pk>\d+)', views.AppVersionCreateView.as_view(),
        name='app-new-version'),
    url(r'^$', views.AppListView.as_view(), name='app-list'),
    url(r'^(?P<pk>\d+)/$', views.AppDetailView.as_view(), name='app_detail'),
    url(r'^download/(?P<pk>[a-zA-Z0-9_-]+)/$',
        views.DownloadTarballView.as_view(),
        name='app-download')
]
