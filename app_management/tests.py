import os
import shutil

from django_webtest import WebTest
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.test import override_settings
from .models import App, AppVersion


class TestApp(WebTest):

    def setUp(self):
        # pylint: disable=no-member
        group = Group.objects.get(name='validated_user')
        self.user = get_user_model().objects.create_user(
            'user_1', 'user_1@test.com', 'pass')
        group.user_set.add(self.user)
        self.user_2 = get_user_model().objects.create_user(
            'user_2', 'user_2@test.com', 'pass')
        group.user_set.add(self.user_2)

    def test_create_view(self):
        page = self.app.get('/app/create', user='user_1')
        self.assertEqual(len(page.forms), 1)

    def test_create_view_error(self):
        page = self.app.get('/app/create', user='user_1')
        page = page.form.submit()
        self.assertContains(page, "This field is required")

    def test_create_view_success(self):
        page = self.app.get('/app/create', user='user_1')
        page.form['title'] = 'foo'
        page = page.form.submit()
        app = App.objects.all()[0]
        self.assertRedirects(page, app.get_absolute_url())
        self.assertIsNotNone(app.created_date)
        self.assertEqual(app.author.username, 'user_1')

    def test_create_with_version_success(self):
        page = self.app.get('/app/create', user='user_1')
        page.form['title'] = 'foo'
        page.form['versions-0-semantic_version'] = '0.1'
        page.form['versions-0-tarball'] = ('my_tarball.tgz', b'foobar',
                                           'application/x-gzip')
        page = page.form.submit()
        app = App.objects.all()[0]
        self.assertEqual(len(app.versions.all()), 1)
        app_version = app.versions.all()[0]
        self.assertEqual(app_version.semantic_version, '0.1')

    def test_create_with_version_error(self):
        page = self.app.get('/app/create', user='user_1')
        page.form['title'] = 'foo'
        page.form['versions-0-semantic_version'] = '0.1'
        page = page.form.submit()
        self.assertContains(page, "This field is required")

    def test_update_view(self):
        app = App(author=self.user, title='app_1')
        app.save()
        page = self.app.get('/app/update/%d' % app.pk, user=self.user)
        self.assertEqual(len(page.forms), 1)

    def test_update_view_success(self):
        app = App(author=self.user, title='app_1')
        app.save()
        page = self.app.get('/app/update/%d' % app.pk, user=self.user)
        page.form['webpage'] = 'http://foo.com'
        page.form.submit()
        # pylint: disable=no-member
        app.refresh_from_db()
        self.assertEqual(app.webpage, 'http://foo.com')

    def test_list(self):
        app = App(author=self.user, title='app_1')
        app.save()
        app_2 = App(author=self.user_2, title='app_2')
        app_2.save()
        page = self.app.get('/app/', user='user_1')
        app_nodes = page.html.find_all('tr')
        # Account for header row
        self.assertEqual(len(app_nodes), 2)
        self.assertIn('app_1', app_nodes[1].text)

    def test_absolute_url(self):
        app = App(author=self.user, title='app_1')
        app.save()
        self.assertIsNotNone(app.get_absolute_url())

    def test_detail_view(self):
        app = App(author=self.user, title='app_1')
        app.save()
        self.app.get(app.get_absolute_url(), user='user_1', status=200)
        self.app.get(app.get_absolute_url(), user='user_2', status=404)

    def test_admin_title(self):
        app = App(author=self.user, title='app_1')
        app.save()
        self.assertEqual(str(app), 'app_1')

    def test_validated_user_required(self):
        get_user_model().objects.create_user(
            'user_3', 'user_3@test.com', 'pass')
        page = self.app.get('/app/', user='new_user', status=403)
        error = page.html.find('div', {'class': 'alert-danger'})
        self.assertIsNotNone(error)

    def test_login_required(self):
        page = self.app.get('/app/', status=302)
        self.assertEqual(page.url, '/accounts/login/?next=/app/')

    def test_unique_slug(self):
        app = App(author=self.user, title='app_1')
        app.save()

        self.assertEqual(app.slug, 'app_1-1')

        app2 = App(author=self.user, title='app_1')
        app2.save()

        self.assertEqual(app2.slug, 'app_1-2')


class TestAppVersion(WebTest):

    def setUp(self):
        # pylint: disable=no-member
        group = Group.objects.get(name='validated_user')
        self.user = get_user_model().objects.create_user(
            'user_1', 'user_1@test.com', 'pass')
        group.user_set.add(self.user)
        self.user_2 = get_user_model().objects.create_user(
            'user_2', 'user_2@test.com', 'pass')
        group.user_set.add(self.user_2)

        self.superuser = get_user_model().objects.create_superuser(
            'admin', 'admin@test.com', 'pass')
        here = os.path.dirname(__file__)
        self.__tmp_dir = os.path.join(here, 'tmp-test')
        shutil.rmtree(self.__tmp_dir, ignore_errors=True)
        os.mkdir(self.__tmp_dir)

    def tearDown(self):
        shutil.rmtree(self.__tmp_dir, ignore_errors=True)

    def test_create_view(self):
        app = App(author=self.user, title='app_1')
        app.save()
        page = self.app.get('/app/new_version/%s' % app.pk, user='user_1')
        self.assertEqual(len(page.forms), 1)

    def test_no_such_app(self):
        self.app.get('/app/new_version/1', user='user_1', status=404)

    def test_invalid_user(self):
        app = App(author=self.user, title='app_1')
        app.save()
        self.app.get('/app/new_version/%s' % app.pk, user='user_2',
                     status=404)

    def test_add_version_success(self):
        app = App(author=self.user, title='app_1')
        app.save()
        page = self.app.get('/app/new_version/%s' % app.pk, user='user_1')
        page.form['semantic_version'] = '0.1'
        page.form['tarball'] = ('my_tarball.tgz', b'foobar',
                                'application/x-gzip')
        page = page.form.submit()
        self.assertRedirects(page, app.get_absolute_url())
        app_version = AppVersion.objects.all()[0]
        self.assertIsNotNone(app_version.uploaded_date)
        self.assertEqual(app_version.uploader.username, 'user_1')
        self.assertFalse(app_version.accepted)

        with open(app_version.tarball.path, 'r') as _:
            contents = _.read()
            self.assertEqual(contents, 'foobar')

    def test_download(self):
        app = App(author=self.user, title='app_1')
        app.save()
        page = self.app.get('/app/new_version/%s/' % app.pk, user='user_1')
        page.form['semantic_version'] = '0.1'
        page.form['tarball'] = ('my_tarball.tgz', b'foobar',
                                'application/x-gzip')
        page.form.submit()
        app_version = AppVersion.objects.all()[0]
        dl_url = app_version.get_tarball_link()
        self.app.get(dl_url, user='user_1', status=200)
        self.app.get(dl_url, user='user_2', status=404)
        self.app.get(dl_url, user='admin', status=200)
        self.app.reset()
        # Non logged-in users get redirected to the login page
        self.app.get(dl_url, status=302)
        page = self.app.get('%s0/' % dl_url[:-1], user='admin', status=404)

    def test_tarball_link(self):
        app = App(author=self.user, title='app_1')
        app.save()
        app_version = AppVersion(
            uploader=self.user, app=app, semantic_version='0.1')
        app_version.tarball.name = 'my_tarball.tgz'
        self.assertIsNone(app_version.get_tarball_link())
        self.assertEqual(app_version.tarball_link(),
                         'No tarball to download')
        app_version.save()
        self.assertIsNotNone(app_version.get_tarball_link())
        self.assertIn('Download tarball', app_version.tarball_link())

    def test_admin_title(self):
        app = App(author=self.user, title='app_1')
        app.save()
        app_version = AppVersion(
            uploader=self.user, app=app, semantic_version='0.1')
        app_version.tarball.name = 'my_tarball.tgz'
        app_version.save()
        self.assertEqual(str(app_version), 'app_1 0.1')

    def test_incorrect_mime_type(self):
        app = App(author=self.user, title='app_1')
        app.save()
        page = self.app.get('/app/new_version/%s/' % app.pk, user='user_1')
        page.form['semantic_version'] = '0.1'
        page.form['tarball'] = ('my_tarball.tgz', b'foobar',
                                'application/x-foobar')
        page = page.form.submit()
        self.assertEqual(len(page.form.html.find_all(
            'div', {'class': 'has-error'})), 1)

    @override_settings(BACKWORTH_MAX_APP_BUNDLE_SIZE=10)
    def test_too_large(self):
        app = App(author=self.user, title='app_1')
        app.save()
        page = self.app.get('/app/new_version/%s/' % app.pk, user='user_1')
        page.form['semantic_version'] = '0.1'
        page.form['tarball'] = ('my_tarball.tgz', b'foobar' * 10,
                                'application/x-gzip')
        page = page.form.submit()
        self.assertEqual(len(page.forms), 1)
        self.assertEqual(len(page.form.html.find_all(
            'div', {'class': 'has-error'})), 1)
