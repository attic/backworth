import os
from datetime import date

from django.db import models
from django.utils import timezone
from django.core.urlresolvers import reverse
from django.conf import settings
from django.utils.text import slugify
from .checked_filefield import ContentTypeRestrictedFileField


def gen_app_slug(instance):
    initial = slugify(instance.title)

    cnt = 1
    instance.slug = '%s-%d' % (initial, cnt)

    while App.objects.filter(slug=instance.slug).exists():
        cnt += 1
        instance.slug = '%s-%d' % (initial, cnt)


class App(models.Model):
    # We don't enforce uniqueness on app titles
    title = models.CharField(
        max_length=200,
        help_text=('The display name for your app. '
                   'It does not have to be unique, but it is better '
                   'if it is (mandatory)'))
    slug = models.SlugField(unique=True)
    author = models.ForeignKey('auth.User')
    created_date = models.DateTimeField(blank=True, null=True)
    webpage = models.URLField(
        blank=True,
        help_text='Link to the webpage for this application (optional)')
    privacy_policy = models.URLField(
        blank=True,
        help_text='Link to the privacy policy for this application (optional)')

    (OTHER,
     COMMUNICATION,
     ENTERTAINMENT,
     NAVIGATION,
     MULTIMEDIA) = range(5)

    CATEGORY_CHOICES = (
        (OTHER, 'Other'),
        (COMMUNICATION, 'Communication'),
        (ENTERTAINMENT, 'Entertainment'),
        (NAVIGATION, 'Navigation'),
        (MULTIMEDIA, 'Multimedia'),
    )

    category = models.IntegerField(
        choices=CATEGORY_CHOICES,
        default=OTHER,
        help_text=('Pick the category that defines your '
                   'application best (mandatory)'))

    description = models.TextField(
        max_length=10000, blank=True,
        help_text=('A description of your application, with a '
                   'maximum of 10 000 characters (optional)'))

    def get_absolute_url(self):
        return reverse('app:app_detail', kwargs={'pk': self.pk})

    def save(self, *args, **kwargs):
        # We do not use add_now, for the reason why see:
        # http://stackoverflow.com/questions/1737017/django-auto-now-and-auto-now-add/1737078#1737078
        if not self.id:  # pragma: no cover
            self.created_date = timezone.now()
            gen_app_slug(self)

        # pylint: disable=no-member
        return super(App, self).save(*args, **kwargs)

    def __str__(self):
        return self.title


def tarball_path_from_filename(instance, filename):
    path = os.path.join(
        instance.app.author.username,
        instance.app.title, instance.app.slug, 'tarballs',
        date.today().strftime('%Y/%m/%d'), os.path.basename(filename))

    final_path = path
    ctr = 1
    while os.path.exists(os.path.join(settings.MEDIA_ROOT, final_path)):
        final_path = '%s-%d' % (path, ctr)
        ctr += 1

    return final_path


class AppVersion(models.Model):
    semantic_version = models.CharField(
        max_length=200,
        help_text='For example 0.1, 8.2.5')
    uploader = models.ForeignKey('auth.User')
    uploaded_date = models.DateTimeField(blank=True, null=True)
    app = models.ForeignKey(App, related_name='versions')
    tarball = ContentTypeRestrictedFileField(
        upload_to=tarball_path_from_filename,
        content_types=['application/x-gzip',
                       'application/x-tar',
                       'application/zip',
                       'application/x-zip',
                       'application/x-zip-compressed'])
    accepted = models.BooleanField(default=False)

    def get_absolute_url(self):
        return reverse('app:app_detail', kwargs={'pk': self.app.pk})

    def save(self, *args, **kwargs):
        if not self.id:  # pragma: no cover
            self.uploaded_date = timezone.now()
        # pylint: disable=no-member
        return super(AppVersion, self).save(*args, **kwargs)

    def get_tarball_link(self):
        if not self.pk:
            return None
        else:
            return '/app/download/%d/' % self.pk

    def tarball_link(self):
        if self.pk:
            return "<a href='%s'>Download tarball</a>" % \
                self.get_tarball_link()
        else:
            return "No tarball to download"

    tarball_link.allow_tags = True

    def __str__(self):
        return '%s %s' % (str(self.app), self.semantic_version)
