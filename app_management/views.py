from django.views.generic.edit import CreateView, UpdateView
from django.views.generic import DetailView, ListView
from django.core.exceptions import PermissionDenied
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.http import HttpResponseRedirect, Http404
from django_downloadview import ObjectDownloadView
from .models import App, AppVersion
from .forms import AppVersionFormSet, AppForm


class ValidatedUserRequiredMixin(UserPassesTestMixin):
    permission_denied_message = ("New users need to be validated "
                                 "by our staff before accessing this feature")

    def test_func(self):
        return self.request.user.groups.filter(name='validated_user').exists()\
            or self.request.user.is_superuser

    def handle_no_permission(self):
        if self.request.user.is_authenticated():
            raise PermissionDenied(self.permission_denied_message)
        return super(ValidatedUserRequiredMixin, self).handle_no_permission()


class AppCreateView(ValidatedUserRequiredMixin, CreateView):
    model = App
    form_class = AppForm

    def get(self, request, *args, **kwargs):
        # pylint: disable=attribute-defined-outside-init
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        app_version_form = AppVersionFormSet()
        return self.render_to_response(
            self.get_context_data(form=form,
                                  app_version_form=app_version_form))

    def post(self, request, *args, **kwargs):
        # pylint: disable=attribute-defined-outside-init
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        app_version_form = AppVersionFormSet(self.request.POST,
                                             self.request.FILES)
        if form.is_valid() and app_version_form.is_valid():
            return self.form_valid(form, app_version_form)
        else:
            return self.form_invalid(form, app_version_form)

    # pylint: disable=arguments-differ
    def form_valid(self, form, app_version_form):
        app = form.save(commit=False)
        # We're protected by the LoginRequired mixin, user has to be != None
        app.author = self.request.user
        app.save()
        app_version_form.instance = app
        app_versions = app_version_form.save(commit=False)

        for app_version in app_versions:
            app_version.app = app
            app_version.uploader = self.request.user
            app_version.save()

        return HttpResponseRedirect(app.get_absolute_url())

    # pylint: disable=arguments-differ
    def form_invalid(self, form, app_version_form):
        return self.render_to_response(
            self.get_context_data(form=form,
                                  app_version_form=app_version_form))


class AppUpdateView(LoginRequiredMixin, UpdateView):
    model = App
    fields = ['category', 'webpage', 'privacy_policy', 'description']
    template_name_suffix = '_update_form'


class AppDetailView(ValidatedUserRequiredMixin, DetailView):
    model = App

    def get_queryset(self):
        qs = super(AppDetailView, self).get_queryset()
        return qs.filter(author=self.request.user)


class AppListView(ValidatedUserRequiredMixin, ListView):
    model = App

    def get_queryset(self):
        return App.objects.filter(author=self.request.user)


class AppVersionCreateView(LoginRequiredMixin, CreateView):
    model = AppVersion
    fields = ['semantic_version', 'tarball']

    def dispatch(self, *args, **kwargs):
        pk = kwargs.get('pk')

        try:
            app = App.objects.get(pk=pk)
        except App.DoesNotExist:
            raise Http404("App %s does not exist" % pk)

        if self.request.user != app.author:
            raise Http404("App %s does not exist" % pk)

        # pylint: disable=no-member
        # pylint: disable=bad-super-call
        return super(CreateView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        obj = form.save(commit=False)

        pk = self.kwargs.get('pk')
        obj.app = App.objects.get(pk=pk)

        obj.uploader = self.request.user
        obj.save()
        return HttpResponseRedirect(obj.get_absolute_url())


class DownloadTarballView(LoginRequiredMixin, ObjectDownloadView):
    model = AppVersion
    file_field = 'tarball'

    def get(self, *args, **kwargs):
        pk = kwargs.get('pk')

        try:
            app_version = AppVersion.objects.get(pk=pk)
        except AppVersion.DoesNotExist:
            raise Http404("App version %s does not exist" % pk)

        if app_version.app.author != self.request.user and \
                not self.request.user.is_superuser:
            raise Http404("App version %s does not exist" % pk)

        return super(DownloadTarballView, self).get(*args, **kwargs)
