from django.forms import ModelForm
from django.forms.models import inlineformset_factory

from .models import App, AppVersion


class AppForm(ModelForm):

    class Meta:
        model = App
        fields = ['title', 'category', 'webpage', 'privacy_policy',
                  'description']


AppVersionFormSet = inlineformset_factory(
    App, AppVersion, fields=['semantic_version', 'tarball'],
    extra=1, can_delete=False)
