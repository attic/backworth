# Administration of the portal

## Connecting to the admin:

Add `/admin/` to the base url of the website, for example `127.0.0.1:8000/admin/` or `appdev.apertis.org/admin/`.

You obviously need a superuser account.

## Quickly listing non-validated users:

In the admin, click on `Users`, then in the right-hand side panel click on `By groups > -`. You can filter out superusers by clicking on `By superuser status > no`

## Validating a user

Click on the user you wish to validate, review their profile then in `Groups`, select `validated_user`, click on the `Choose` right arrow which is now enabled, then click `save` at the bottom of the page.

When a user has been validated, they can create and update their own applications, and propose new versions for them.
