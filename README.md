# Apertis developer portal

Codename: backworth

The scope of this project is as follows:

* Allow developers to download the Apertis SDK
* Provide developers with documentation on how to use the SDK, and with various API reference manuals
* Let authenticated developers or organisations upload their application
* Validate uploaded applications
* Let developers manage and analyse the applications they uploaded
* Give administrators complete access to and control over the uploaded applications

## Setup

### Install system-wide dependencies

You will need git, python3 and virtualenv to be installed.

On Fedora, python3 is already installed by default, to satisfy the rest of the requirements run:

```
sudo dnf install git
sudo dnf install pip
sudo pip install virtualenv
```

### Install the rest of the dependencies

```
git clone backworth
git submodule update --init
virtualenv -p python3 backworth_env
source backworth_env/bin/activate
cd backworth
pip install -r requirements.txt
```

### Define local settings

The main settings file expects a `local_settings.py` file to be importable, from which it will gather environment-specific variables.

A sample file is provided, `local_settings_sample.py`, copy it that way:

```
cp backworth/local_settings_sample.py backworth/local_settings.py
```

Then edit it according to the comments in the file, do not version control it (it is ignored by git anyway)

### Build the documentation

Follow the instructions in the "Building the documentation for usage on appdev.apertis.org" section of https://git.apertis.org/cgit/apertis-docs.git/tree/README.md

### Setup backworth itself

```
python manage.py migrate
python manage.py collectstatic
python manage.py createsuperuser
```

## Run

```
python manage.py runserver
```

You can then navigate to 127.0.0.1:8000 to view the website, and connect to the admin interface through 127.0.0.1:8000/admin/ .

### Temporary sign-in sign-up testing

To test sign in and sign up, first log out from the admin interface with the "log out" button on the top right, then navigate to 127.0.0.1:8000/accounts/login

Successful authentication will simply redirect to the home page for now, you can have a look at the created users in the admin interface.

For testing the user signup and email verification you will have to setup a debug email server:

```
python3 -m smtpd -n -c DebuggingServer localhost:1025
```

## Contribute

Have a look at HACKING.md for specific instructions on how to contribute.

### Licensing

Backworth is licensed under the Mozilla Public License Version 2.0. See COPYING for more details.
