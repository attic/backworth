from django.conf.urls import include, url
from django.contrib import admin


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^app/', include('app_management.urls')),
    url(r'^documentation/', include('documentation.urls')),
    url(r'^', include('main.urls')),
]
