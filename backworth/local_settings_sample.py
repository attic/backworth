BACKWORTH_PRODUCTION = False

# Change this in production!
# http://www.miniwebtool.com/django-secret-key-generator/
SECRET_KEY = '0vn+jh-y^k#5-7dx(thbmyoj*e$w!f=#6y-9pse53@85b-ok67'

# Modify this to distribute another version of the SDK

BACKWORTH_SDK_DOWNLOAD_LINK = "https://images.apertis.org/release/16.03/images/sdk/amd64-uefi/16.03.0/apertis-16.03-sdk-amd64-uefi_16.03.0.btrfs.vdi.gz"  # noqa

# Email settings, shouldn't look like that in production
EMAIL_HOST = 'localhost'
EMAIL_PORT = 1025
DEFAULT_FROM_EMAIL = 'webmaster@localhost'

# Allauth settings, uncomment in production

# ACCOUNT_EMAIL_VERIFICATION = ("mandatory")
# ACCOUNT_UNIQUE_EMAIL = True
# ACCOUNT_USERNAME_REQUIRED = False
# ACCOUNT_EMAIL_REQUIRED = True

# Edit this with actual keys

RECAPTCHA_PUBLIC_KEY = None
RECAPTCHA_PRIVATE_KEY = None
