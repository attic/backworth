## Updating dependencies

In a clean environment, once a dependency has been added, use:

```
pip freeze -r requirements.txt > requirements.txt
```

to update the dependencies.

## Making changes

The requirements install two commit hooks automatically, before committing changes in backworth you should run the following command in the project top directory:

```
ln -s $PWD/pre-commit .git/hooks/pre-commit
```

Keeping the test coverage to 100 % is a requirement, see below for instructions on this.

Following a test-driven development approach is encouraged as well, here is a good tutorial for TDD with django: http://test-driven-django-development.readthedocs.org/en/latest/index.html .

Note that the django-pylint plugin seems to have some issues, see https://github.com/landscapeio/pylint-django/issues/53 for more info, I have not yet encountered this problem enough times to completely convince me to disable testing for django members altogether, for now, when certain the issue is related simply use such a comment:

```
# pylint: disable=no-member
```

on the line that shows the issue.

We use modern django, in particular we prefer class-based views to functional views, take a look at the app_management application for inspiration.

## Running the tests:

Tests can be run with:

```
python manage.py test <application_name>
```

For example, `python manage.py test app_management`.

A .coveragerc file is provided, and coverage is part of the requirements, for more information on how to run test coverage reports see http://test-driven-django-development.readthedocs.org/en/latest/06-the-testing-game.html
