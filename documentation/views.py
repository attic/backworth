import os
import io

from django.views.generic.base import TemplateView, RedirectView, View
from django.http import Http404, HttpResponse
from django.conf import settings


class DocumentationRedirectView(RedirectView):

    def get(self, request, *args, **kwargs):
        path = request.path
        self.url = settings.STATIC_URL + path
        return super(DocumentationRedirectView, self).get(
            request, args, **kwargs)


class DocumentationRawView(View):

    def get(self, request, *args, **kwargs):
        path = settings.STATIC_ROOT + request.path
        if os.path.exists(path):
            with open(path, 'r') as _:
                res = _.read()
            return HttpResponse(res)

        raise Http404('Raw file not found at %s' % request.path)


class DocumentationView(TemplateView):

    template_name = 'documentation/documentation_page.html'

    def get(self, *args, **kwargs):
        hd_folder = os.path.join(settings.STATIC_ROOT, 'documentation')
        pagename = kwargs['pagename']
        path = os.path.join(hd_folder, pagename)

        if not os.path.exists(path):
            raise Http404('Documentation page %s not found in'
                          '%s' % (pagename, hd_folder))

        return super(DocumentationView, self).get(*args, **kwargs)

    # pylint: disable=too-many-locals
    def get_context_data(self, **kwargs):
        context = super(DocumentationView, self).get_context_data(**kwargs)
        hd_folder = os.path.join(settings.STATIC_ROOT, 'documentation')
        pagename = kwargs['pagename']
        path = os.path.join(hd_folder, pagename)

        with io.open(path, 'r', encoding='utf-8') as _:
            contents = _.read()

        context['hotdoc_contents'] = contents

        stripped = os.path.splitext(pagename)[0]

        static_assets_url = os.path.join(settings.STATIC_URL, 'documentation',
                                         'assets')

        js_path = os.path.join(hd_folder, stripped + '.scripts')
        scripts = []
        if os.path.exists(js_path):  # pragma: no cover
            with io.open(js_path, 'r', encoding='utf-8') as _:
                for line in _.readlines():
                    line = line.strip()
                    scripts.append(os.path.join(static_assets_url, 'js', line))

        context['hotdoc_scripts'] = scripts

        css_path = os.path.join(hd_folder, stripped + '.stylesheets')
        stylesheets = []
        if os.path.exists(css_path):  # pragma: no cover
            with io.open(css_path, 'r', encoding='utf-8') as _:
                for line in _.readlines():
                    line = line.strip()
                    stylesheets.append(os.path.join(static_assets_url,
                                                    'css', line))

        context['hotdoc_stylesheets'] = stylesheets

        return context
