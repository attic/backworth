from django.conf.urls import url

from . import views

app_name = 'documentation'

urlpatterns = [
    url(r'^media/(?P<filename>.+)', views.DocumentationRedirectView.as_view()),
    url(r'^assets/js/search/hotdoc_fragments/(?P<filename>.+)',
        views.DocumentationRawView.as_view()),
    url(r'^assets/(?P<filename>.+)',
        views.DocumentationRedirectView.as_view()),
    url(r'^dumped.trie', views.DocumentationRedirectView.as_view()),
    url(r'^(?P<pagename>.+)', views.DocumentationView.as_view(),
        name='documentation_page'),
]
