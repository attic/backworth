import os
import shutil

from django.contrib.auth import get_user_model
from django.test import override_settings
from django_webtest import WebTest

HERE = os.path.dirname(__file__)
TMP_DIR = os.path.join(HERE, 'tmp-test')
STATIC_DOC_DIR = os.path.join(TMP_DIR, 'documentation')
STATIC_MEDIA_DIR = os.path.join(STATIC_DOC_DIR, 'media')


@override_settings(STATIC_ROOT=TMP_DIR)
class TestDoc(WebTest):

    def setUp(self):
        shutil.rmtree(TMP_DIR, ignore_errors=True)
        os.makedirs(STATIC_MEDIA_DIR)
        self.user = get_user_model().objects.create_user(
            'user_1', 'user_1@test.com', 'pass')

    def tearDown(self):
        shutil.rmtree(TMP_DIR, ignore_errors=True)

    def test_doc_view_no_such_page(self):
        self.app.get('/documentation/my_page.html',
                     user=self.user, status=404)

    def test_doc_view_success(self):
        with open(os.path.join(STATIC_DOC_DIR, 'site_navigation.html'),
                  'w') as _:
            _.write('')

        with open(os.path.join(STATIC_DOC_DIR, 'my_page.html'), 'w') as _:
            _.write('<div class="testdoc"></div>')

        with open(os.path.join(STATIC_DOC_DIR, 'my_page.scripts'),
                  'w') as _:
            _.write('test.js\n')

        with open(os.path.join(STATIC_DOC_DIR, 'my_page.stylesheets'),
                  'w') as _:
            _.write('test.css\n')

        page = self.app.get('/documentation/my_page.html', user=self.user)

        self.assertEqual(len(page.html.findAll('div', {'class': 'testdoc'})),
                         1)

        script = page.html.findAll('script')[-1]
        self.assertEqual(script['src'],
                         os.path.join('/', 'static', 'documentation', 'assets',
                                      'js', 'test.js'))

        expected_href = os.path.join('/', 'static', 'documentation', 'assets',
                                     'css', 'test.css')

        stylesheet = page.html.find('link', {'href': expected_href})
        self.assertIsNotNone(stylesheet)

    def test_media_redirect(self):
        page = self.app.get('/documentation/media/foo.png', user=self.user)
        self.assertEqual(
            os.path.abspath(page.url),
            '/static/documentation/media/foo.png')
