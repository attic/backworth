from django import forms
from captcha.fields import ReCaptchaField


class SdkTermsForm(forms.Form):
    terms = forms.BooleanField(
        error_messages={'required':
                        'You must accept the terms and conditions'},
        label="I have read and accept the terms and conditions"
    )


class AllauthSignupForm(forms.Form):

    captcha = ReCaptchaField()

    def signup(self, request, user):
        pass
