from django.apps import AppConfig
from django.template.loader import render_to_string
from django.db.models.signals import post_migrate, m2m_changed
from django.core.mail import EmailMessage


def create_groups_cb(sender, **kwargs):
    from django.contrib.auth.models import Group, Permission

    # pylint: disable=no-member
    Group.objects.get_or_create(name='validated_user')


def user_group_changed_cb(sender, **kwargs):
    users = []

    if kwargs['action'] not in ('post_remove', 'post_add'):
        return

    if kwargs['reverse']:
        users = [kwargs['model'].objects.get(pk=pk) for pk in kwargs['pk_set']]
    else:
        users = [kwargs['instance']]

    for user in users:
        if user.groups.filter(name='validated_user').exists():
            subject = 'Congratulations, your Apertis account was validated'
            to = [user.email]
            ctx = {'user': user}
            message = render_to_string('email/user_validated_email.txt', ctx)
            EmailMessage(subject, message, to=to).send()
        else:
            subject = 'Unfortunately, your Apertis account was invalidated'
            to = [user.email]
            ctx = {'user': user}
            message = render_to_string('email/user_invalidated_email.txt', ctx)
            EmailMessage(subject, message, to=to).send()


class MainConfig(AppConfig):
    name = 'main'

    def ready(self):
        from django.contrib.auth.models import User

        post_migrate.connect(create_groups_cb, sender=self)
        # pylint: disable=no-member
        m2m_changed.connect(user_group_changed_cb, sender=User.groups.through)
