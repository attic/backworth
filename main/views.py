# pylint: disable=arguments-differ
from django.views.generic import FormView, UpdateView
from django.views.generic.base import TemplateView
from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.models import User

from .forms import SdkTermsForm


class SdkTermsView(LoginRequiredMixin, FormView):
    form_class = SdkTermsForm
    template_name = 'sdk_terms.html'
    success_url = '/sdk-download'


class SdkDownloadView(LoginRequiredMixin, TemplateView):
    template_name = 'sdk_download.html'

    def get_context_data(self, **kwargs):
        context = super(SdkDownloadView, self).get_context_data(**kwargs)
        context['download_link'] = settings.BACKWORTH_SDK_DOWNLOAD_LINK
        return context


class ProfileView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = User
    fields = ['first_name', 'last_name']
    template_name = 'profile.html'
    success_message = "Profile successfully updated"
    success_url = '/profile'

    def get_object(self):
        return self.request.user
