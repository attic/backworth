import os

from django.core import mail
from django.test import TestCase
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.test import override_settings
from django_webtest import WebTest


class TestMain(WebTest):

    def setUp(self):
        self.user = get_user_model().objects.create_user('user_1',
                                                         'user_1@test.com',
                                                         'pass')

    def test_create_view(self):
        page = self.app.get('/sdk', user=self.user)
        self.assertEqual(len(page.forms), 1)

    def test_accept_required(self):
        page = self.app.get('/sdk', user=self.user)
        page = page.form.submit()
        self.assertContains(page, "You must accept the terms and conditions")

    def test_success(self):
        page = self.app.get('/sdk', user=self.user)
        page.form['terms'] = True
        page = page.form.submit()
        self.assertRedirects(page, '/sdk-download')


# These two settings are disabled in development
@override_settings(ACCOUNT_EMAIL_REQUIRED=True,
                   ACCOUNT_EMAIL_VERIFICATION='mandatory')
class TestApp(WebTest):
    def setUp(self):
        mail.outbox = []

    def test_registration_form(self):
        form = self.app.get('/accounts/signup/').form
        form['username'] = 'foo'
        form['email'] = 'mail@example.com'
        form['password1'] = 'bdsafafaar'
        form['password2'] = 'bdsafafaar'
        form['recaptcha_response_field'] = 'PASSED'

        os.environ['RECAPTCHA_TESTING'] = 'True'
        response = form.submit()
        os.environ['RECAPTCHA_TESTING'] = 'False'

        self.assertRedirects(response, '/accounts/confirm-email/',
                             host=None,
                             msg_prefix='',
                             fetch_redirect_response=True)
        self.assertNotEqual(response.context["user"], "foo")
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject,
                         '[example.com] Please Confirm Your E-mail Address')


class EmailTest(TestCase):

    def setUp(self):
        mail.outbox = []

    def test_send_email(self):
        mail.send_mail('Subject here', 'Here is the message.',
                       'webmaster@localhost', ['mail@example.com'],
                       fail_silently=False)

        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, 'Subject here')


class TestGroups(TestCase):

    def setUp(self):
        mail.outbox = []

    def test_new_user(self):
        new_user = get_user_model().objects.create_user('user_1',
                                                        'user_1@test.com',
                                                        'pass')
        self.assertEqual(len(new_user.groups.all()), 0)

    def test_group_exists(self):
        # pylint: disable=no-member
        self.assertTrue(Group.objects.filter(name='validated_user').exists())

    def test_user_validated(self):
        # pylint: disable=no-member
        group = Group.objects.get(name='validated_user')
        new_user = get_user_model().objects.create_user(
            'user_1', 'user_1@test.com', 'pass')

        group.user_set.add(new_user)

        self.assertTrue(new_user.groups.filter(name='validated_user').exists())

        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(
            mail.outbox[0].subject,
            'Congratulations, your Apertis account was validated')

        mail.outbox = []

        group.user_set.remove(new_user)

        self.assertFalse(new_user.groups.filter(name='validated_user')
                         .exists())
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(
            mail.outbox[0].subject,
            'Unfortunately, your Apertis account was invalidated')

        mail.outbox = []

        # We just check that the reverse relationship works too

        new_user.groups.add(group)
        self.assertTrue(new_user.groups.filter(name='validated_user').exists())

        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(
            mail.outbox[0].subject,
            'Congratulations, your Apertis account was validated')

        mail.outbox = []


class TestUserPage(WebTest):
    def setUp(self):
        self.user = get_user_model().objects.create_user('user_1',
                                                         'user_1@test.com',
                                                         'pass')
        self.user.first_name = 'dsa'
        self.user.last_name = 'das'

    def test_get_page(self):
        page = self.app.get('/profile', user=self.user)
        self.assertEqual(page.status_code, 200)

    def test_change_names(self):
        form = self.app.get('/profile', user=self.user).form
        form['first_name'] = 'foo'
        form['last_name'] = 'bar'
        form.submit()
        self.user.refresh_from_db()
        self.assertEqual(self.user.first_name, 'foo')
        self.assertEqual(self.user.last_name, 'bar')
