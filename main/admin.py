from django.contrib import admin
from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin


def validated(self):  # pragma: no cover
    return self.groups.filter(name='validated_user').exists()

validated.boolean = True
validated.admin_order_field = 'is_validated'


class MainUserAdmin(UserAdmin):
    list_display = UserAdmin.list_display + (validated,)

admin.site.unregister(User)
admin.site.register(User, MainUserAdmin)
