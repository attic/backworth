from django.conf.urls import url
from django.views.generic.base import TemplateView

from . import views


urlpatterns = [
    url(r'^sdk$', views.SdkTermsView.as_view(), name='sdk_terms'),
    url(r'^sdk-download$', views.SdkDownloadView.as_view(),
        name='sdk_download'),
    url(r'^profile', views.ProfileView.as_view(),
        name='user_home'),
    url(r'^$',
        TemplateView.as_view(template_name='home.html'),
        name='home'),
]
